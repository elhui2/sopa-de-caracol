var index = 12, chanfle, message, round = 1;

var tiempo = {
    hora: 0,
    minuto: 0,
    segundo: 0
};

$(document).ready(function () {
    $(document).foundation();

    $("#btn-comenzar").click(function () {

        if ($(this).text() == 'Comenzar')
        {
            $(this).text('Detener');

            setWord();
            console.debug('Reloj Comenzar');
            reloj();
        }
        else
        {
            $(this).text('Comenzar');
            clearInterval(chanfle);
        }
    });

    message = $('#message');

    $('#check-word').click(checkWord);

    message.bind('closed.zf.reveal', function () {
        if (index < 15) {
            tiempo.segundo = 30;
            $("#second").text(30);
            setWord();
            console.debug('Reloj Cerrar'+index);
            reloj();
        }else{
            index = 0;
        }
    });
});

function reloj() {
    console.debug('Reloj');
    tiempo.segundo = 30;
    chanfle = setInterval(function () {
        $("#second").text(tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);
        tiempo.segundo--;

        if (tiempo.segundo === 0) {
            checkWord();
        }
    }, 1000);
}

function setWord() {

    $('#game-canvas').html('');

    var palabra = juegos[index].palabra;
    var chars = palabra.split('');
    var charsRandom = shuffleArray(chars);
    $('#pista').html(juegos[index].pista);
    for (var x = 0; x < charsRandom.length; x++) {
        $('#game-canvas').append('<div class="item" value="' + charsRandom[x] + '">' + charsRandom[x] + '</div>');
    }

    setTimeout(function () {
        $("#game-canvas").sortable({
            revert: true
//            stop: checkWord()
        });
    }, 500);

}

function checkWord() {
    console.debug('checkWord' + index);

    clearInterval(chanfle);

    var readWord = '';
    $('#game-canvas .item').each(function () {
        var item = $(this);
        readWord += item.attr('value');
    });


    if (readWord == juegos[index].palabra) {
        $('#message h1').text('lo Hiciste');
        $('#message img').attr('src', 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Bien Hecho!&w=640&h=360');
        $('#round' + round + '-label').html('<i class="fi-check"></i>');
    } else {
        $('#message h1').text('Intenta una vez más');
        $('#message img').attr('src', 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Incorrecto!&w=640&h=360');
        $('#round' + round + '-label').html('<i class="fi-x"></i>');
    }

    $('#round' + round).prop("checked", true);

    message.foundation('open');

    if (index === 14) {
        endGame();
    }

    if (round === 3) {
        endRound();
    }

    round++;

    index++;
}

function endRound() {
    round = 0;
    $('#message h1').text('Gracias por participar');
    $('#message img').attr('src', 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Incorrecto!&w=640&h=360');
    $('#round1-label').html('<i class="fi-eye"></i>');
    $('#round2-label').html('<i class="fi-eye"></i>');
    $('#round3-label').html('<i class="fi-eye"></i>');
    $('#round1').prop("checked", false);
    $('#round2').prop("checked", false);
    $('#round3').prop("checked", false);

}

function endGame() {
    clearInterval(chanfle);
    round = 0;
    $('#message h1').text('Fin del Juego');
    $('#message img').attr('src', 'https://placeholdit.imgix.net/~text?txtsize=33&txt=Incorrecto!&w=640&h=360');
    $('#round1-label').html('<i class="fi-eye"></i>');
    $('#round2-label').html('<i class="fi-eye"></i>');
    $('#round3-label').html('<i class="fi-eye"></i>');
    $('#round1').prop("checked", false);
    $('#round2').prop("checked", false);
    $('#round3').prop("checked", false);
    $('#game-canvas').html('');
    $('#btn-comenzar').text('Comenzar');
    $("#second").text('00');
    index++;
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

var juegos = [
    {
        "palabra": "Planear",
        "pista": "<strong>Pista:</strong> Trazar la ruta para encontrar el mejor camino que te ayude a lograr lo que te propongas.",
        "image": "images_tmp/planeacion.jpg",
    },
    {
        "palabra": "Vejez",
        "pista": "<strong>Pista:</strong> Último periodo de la vida de una persona en el cual sus ingresos disminuyen."
    },
    {
        "palabra": "Fondo de ahorro",
        "pista": "<strong>Pista:</strong> Ahorro destinado emergencia, imprevistos o el retiro."
    },
    {
        "palabra": "Planeación",
        "pista": "<strong>Pista:</strong> Consiste en elaborar un plan y trazar los pasos para conseguir las metas que se fijen."
    },
    {
        "palabra": "Pensión",
        "pista": "<strong>Pista:</strong> Cantidad de dinero que alguien recibe al finalizar su actividad laboral."
    },
    {
        "palabra": "Administración",
        "pista": "<strong>Pista:</strong> te ayuda a mayor control de las entradas y salidas de tu dinero para hacerlo hacerlo rendir."
    },
    {
        "palabra": "Invertir",
        "pista": "<strong>Pista:</strong> Poner a trabajar tu dinero para hacerlo crecer en el futuro."
    },
    {
        "palabra": "Ahorrar",
        "pista": "<strong>Pista:</strong> Separar y guardar parte de tu dinero para utilizarlo en el futuro."
    },
    {
        "palabra": "Retiro",
        "pista": "<strong>Pista:</strong> Etapa de la vida cuándo finalizas tu ciclo laboral."
    },
    {
        "palabra": "Prevenir",
        "pista": "<strong>Pista:</strong> Tomar precauciones o medidas por adelantado para evitar un daño, un riesgo que afecte tu bolsillo."
    },
    {
        "palabra": "Planes",
        "pista": "<strong>Pista:</strong> Metas que son tu motivación para lograr en el futuro."
    },
    {
        "palabra": "Seguridad",
        "pista": "<strong>Pista:</strong> Sensación de total confianza que se tiene cuando se cuenta con un fondo de ahorro para cubrir gastos."
    },
    {
        "palabra": "Emergencia",
        "pista": "<strong>Pista:</strong> Suceso que acontece de manera absolutamente imprevista y puede desajustar tus finanzas."
    },
    {
        "palabra": "AFORE",
        "pista": "<strong>Pista:</strong> Administradora de fondos de ahorro para el retiro que te administra tu dinero hasta finalizar tu etapa laboral."
    },
    {
        "palabra": "Aportaciones voluntarias",
        "pista": "<strong>Pista:</strong> Depósitos adicionales que realizas para hacer crecer tu ahorro para el retiro."
    }
]